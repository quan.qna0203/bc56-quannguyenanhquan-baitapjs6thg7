function ketQua() {
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value;
    var diemMon1 = document.getElementById("diemMon1").value * 1;
    var diemMon2 = document.getElementById("diemMon2").value * 1;
    var diemMon3 = document.getElementById("diemMon3").value * 1;
    var diemThi = (diemMon1 + diemMon2 + diemMon3);
    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
        diemThi = 0;
    }
    var diemChuan = document.getElementById("diemChuan").value * 1;
    var diemTong = 0 * 1;
    if (diemThi != 0) {
        switch (khuVuc) {
            case "X":
                switch (doiTuong) {
                    case "0":
                        diemTong = diemThi + 0;
                        break;
                    case "1":
                        diemTong = diemThi + 0 + 2.5;
                        break;
                    case "2":
                        diemTong = diemThi + 0 + 1.5;
                        break;
                    case "3":
                        diemTong = diemThi + 0 + 1;
                        break;
                    default:
                        break;
                }
                break;
            case "A":
                switch (doiTuong) {
                    case "0":
                        diemTong = diemThi + 2;
                        break;
                    case "1":
                        diemTong = diemThi + 2 + 2.5;
                        break;
                    case "2":
                        diemTong = diemThi + 2 + 1.5;
                        break;
                    case "3":
                        diemTong = diemThi + 2 + 1;
                        break;
                    default:
                        break;
                }
                break;
            case "B":
                switch (doiTuong) {
                    case "0":
                        diemTong = diemThi + 1;
                        break;
                    case "1":
                        diemTong = diemThi + 1 + 2.5;
                        break;
                    case "2":
                        diemTong = diemThi + 1 + 1.5;
                        break;
                    case "3":
                        diemTong = diemThi + 1 + 1;
                        break;
                    default:
                        break;
                }
                break;
            case "C":
                switch (doiTuong) {
                    case "0":
                        diemTong = diemThi + 0.5;
                        break;
                    case "1":
                        diemTong = diemThi + 0.5 + 2.5;
                        break;
                    case "2":
                        diemTong = diemThi + 0.5 + 1.5;
                        break;
                    case "3":
                        diemTong = diemThi + 0.5 + 1;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    if (diemTong >= diemChuan && diemTong != 0) {
        document.getElementById("resultbai1").innerHTML = `
        <input readonly type="text" value="Điểm tổng: ${diemTong} -  Bạn đã đậu vào trường">
        `;
    } else {
        document.getElementById("resultbai1").innerHTML = `
       <input readonly type="text" value="Điểm tổng: ${diemTong} - Bạn đã không đủ điểm vào trường">
        `;
    }
    // console.log(diemTong)
}

// Bài 2
function mocTienDien(soKw) {
    var soTienPhaiTra = 0;
    console.log(soKw);
    if (soKw > 0 && soKw <= 50) {
        soTienPhaiTra = 500 * soKw;
    } else if (50 < soKw && soKw <= 100) {
        soTienPhaiTra = 50 * 500 + (soKw - 50) * 650;
    } else if (100 < soKw && soKw <= 200) {
        soTienPhaiTra = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    } else if (200 < soKw && soKw <= 350) {
        soTienPhaiTra = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    } else if (350 < soKw) {
        soTienPhaiTra = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300
    }
    return soTienPhaiTra
}
function tinhTien() {
    var tenHoaDon = document.getElementById("tenHoaDon").value;
    var soKw = document.getElementById("soKw").value * 1;
    console.log(soKw);
    var soTien = mocTienDien(soKw);
    document.getElementById("resultbai2").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Số tiền mà hoá đơn ${tenHoaDon} phải trả là ${new Intl.NumberFormat('vn-VN').format(soTien)} 
    </p >
    `;
}

// bai 3
function tinhThuNhapChiuThue(tongThuNhap, soNguoiPhuThuoc) {
    var thuNhapChiuThue = tongThuNhap - 4 - soNguoiPhuThuoc * 1.6;
    return thuNhapChiuThue
}

function tinhThueCacMoc(thuNhapChiuThue) {
    var soTienThue = 0;

    if (0 < thuNhapChiuThue && thuNhapChiuThue <= 60) {
        soTienThue = (5 * thuNhapChiuThue) / 100
    } else if (60 < thuNhapChiuThue <= 120) {
        soTienThue = (10 * thuNhapChiuThue) / 100
    } else if (120 < thuNhapChiuThue <= 210) {
        soTienThue = (15 * thuNhapChiuThue) / 100
    } else if (210 < thuNhapChiuThue <= 384) {
        soTienThue = (20 * thuNhapChiuThue) / 100
    } else if (384 < thuNhapChiuThue <= 624) {
        soTienThue = (25 * thuNhapChiuThue) / 100
    } else if (624 < thuNhapChiuThue <= 960) {
        soTienThue = (30 * thuNhapChiuThue) / 100
    } else if (960 < thuNhapChiuThue) {
        soTienThue = (35 * thuNhapChiuThue) / 100
    }
    return soTienThue
}

function tinhThue() {
    var hoTen = document.getElementById("hoTen").value;
    var tongThuNhap = document.getElementById("tongThuNhap").value * 1;
    var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value * 1;
    var thuNhapChiuThue = tinhThuNhapChiuThue(tongThuNhap, soNguoiPhuThuoc);
    var tienThuePhaiTra = tinhThueCacMoc(thuNhapChiuThue) * 1000000;
    document.getElementById("resultbai3").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Số tiền thuế mà ${hoTen} phải trả là ${new Intl.NumberFormat('vn-VN').format(tienThuePhaiTra)} 
    </p >
    `;
}


// bai 4
function checkLoaiKhach() {
    var loaiKhach = document.getElementById("loaiKhach").value;
    // console.log(loaiKhach)
    if (loaiKhach == 0) {
        document.getElementById("checkKetNoi").innerHTML = `
        <input type="number"
        class="form-control mb-2"
        id="soKetNoi"
        placeholder="Số kết nối" disabled>
        `;
    } else {
        document.getElementById("checkKetNoi").innerHTML = `
        <input type="number"
        class="form-control mb-2"
        id="soKetNoi"
        placeholder="Số kết nối" >
        `;
    }
    
}

function tinhHoaDon() {
    var maKhachHang = document.getElementById("maKhachHang").value
    var loaiKhach = document.getElementById("loaiKhach").value * 1;
    var soKenhCaoCap = document.getElementById("soKenhCaoCap").value * 1;
    var soKetNoi = document.getElementById("soKetNoi").value * 1;
    var soTienPhaiTra =0
    if(loaiKhach==0){
        soTienPhaiTra = 4.5+20.5+soKenhCaoCap*7.5;
    }else{
        if(0<soKetNoi&&soKetNoi<=10){
            soTienPhaiTra = 15+75+50*soKenhCaoCap;
        }else{
            soTienPhaiTra = 15 +75 +(soKetNoi-10)*5+50*soKenhCaoCap
        }
    }
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Số tiền mà ${maKhachHang} phải trả là ${soTienPhaiTra}$ 
    </p >
    `;
}